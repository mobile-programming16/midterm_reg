import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('63160213'),
            accountEmail: Text('63160213@go.buu.ac.th'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://cdn.inprnt.com/thumbs/ac/0e/ac0e270c7692d08045f12b15fab4acc5.jpg',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.grey.shade800,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://play-lh.googleusercontent.com/_17mstfR96IB4LLLsrWXROUOD8NZrtsqpw1oxxpEfIAT2Z0h8XY0ETljcv5NQbszNQ')),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('เมนูหลัก'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text('ลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('ตารางเรียนและตารางสอบ'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.face),
            title: Text('ประวัตินิสิตและผลการศึกษา'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.monetization_on),
            title: Text('ค่าใช้จ่าย'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description),
            title: Text('ยื่นคำร้อง'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('เกี่ยวกับ'),
            leading: Icon(Icons.settings),
            onTap: () => null,
          ),
          ListTile(
            title: Text('ออกจากระบบ'),
            leading: Icon(Icons.exit_to_app),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}
