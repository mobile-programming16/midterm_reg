import 'package:flutter/material.dart';

class ConstrainedBoxExample extends StatelessWidget {
  const ConstrainedBoxExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Center(
            child: Text(
              '** สอบถามข้อมูลที่กองคลังฯ ชั้น 3 อาคารสำนักงานอธิการบดี (ภปร) โทร 038-102157 **',
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.red),
            ),
          ),
          Image.network(
            "https://o.remove.bg/downloads/59d9dca0-7c13-4e87-99a8-d1fb4419a9f3/pink-removebg-preview.png",
            height: 200,
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText1,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText3,
              style: TextStyle(
                  fontSize: 23.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.purple),
            ),
          ),
          Image.network(
              "https://o.remove.bg/downloads/59d9dca0-7c13-4e87-99a8-d1fb4419a9f3/pink-removebg-preview.png"),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500.0),
            child: Text(
              _longText2,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }
}

String _longText1 =
    '**Download ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร';

String _longText2 =
    '**ค่าไฟฟ้าห้องพักนิสิต กองกิจการนิสิต ภาคปลาย ปีการศึกษา 2565 \nประจำเดือน ธันวาคม พ.ศ. 2565 \n >> ตรวจสอบรายชื่อนิสิตค้างชำระค่าไฟฟ้า << \n- นิสิตสามารถตรวจสอบรายชื่อโดยแสกน qrcode ด้านบน หรือทาง facebook : สำนักงานหอพักนิสิต มหาวิทยาลัยบูรพา ';

String _longText3 = '* ประกาศจากสำนักงานหอพักนิสิต';
